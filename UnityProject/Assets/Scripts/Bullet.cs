﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GD
{
    public class Bullet : MonoBehaviour
    {
        private Rigidbody m_AttachedRigidBody = null;

        private void Awake()
        {
            m_AttachedRigidBody = GetComponent<Rigidbody>();
            m_AttachedRigidBody.isKinematic = true;
        }
        public void Shoot(Vector3 a_vDirection)
        {
            m_AttachedRigidBody.isKinematic = false;
            m_AttachedRigidBody.AddForce(a_vDirection * 500.0f);

            Destroy(this.gameObject, 2.0f);
        }
    }
}