﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GD
{
    public class PowerCell : MonoBehaviour
    {

        [SerializeField] private float m_fChargeUpRate = 5.0f;
       

        [SerializeField] private float m_fCurrentCharge = 100f;
        [SerializeField] private TextMesh m_txtMesh;
        private Material m_refMat = null;

        private float m_fRandomDecrease = 5.0f;


        private bool m_bCharging = false;

        private float m_fTValue = 1;
        // Start is called before the first frame update
        void Start()
        {
            m_fRandomDecrease = Random.Range(0.5f, 1.2f);
            m_refMat = GetComponent<Renderer>().material;

        }

        // Update is called once per frame
        void Update()
        {
            if (m_bCharging == true)
            {
                m_fCurrentCharge = Mathf.Clamp(m_fCurrentCharge + 1.0f * Time.deltaTime * m_fChargeUpRate, 0, 100.0f);
            }
            else
            {
                m_fCurrentCharge = Mathf.Clamp(m_fCurrentCharge - 1.0f * Time.deltaTime * m_fRandomDecrease, 0, 100.0f);

            }
            m_txtMesh.text = m_fCurrentCharge.ToString("0.#");

            m_fTValue = Mathf.InverseLerp(0, 100, m_fCurrentCharge);
            m_refMat.SetColor("_Color", Color.Lerp(Color.red, Color.yellow, m_fTValue));
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                m_bCharging = true;
            }

        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Player")
            {
                m_bCharging = false;
            }
        }
    }
}