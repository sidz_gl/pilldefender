﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GD
{
    public class SelectionManager : MonoBehaviour
    {

        [SerializeField] private Material m_HighlightMaterial;

        [SerializeField] private Transform m_transGun = null;
        public bool m_bEnableLook = true;

        private Material m_prevMAterial = null;
        private Highlight m_HitRender = null;
        // Start is called before the first frame update
        void Start()
        {
            Plane a = new Plane();
           
        }

        // Update is called once per frame
        void Update()
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit m_hitInfo;
            if (Physics.Raycast(ray, out m_hitInfo) == true)
            {
                var temp_highlight = m_hitInfo.collider.gameObject.GetComponent<Highlight>();
                if (temp_highlight != null)
                {
                    Reset();
                    m_HitRender = temp_highlight;
                    m_prevMAterial = m_HitRender.GetMaterial();
                    temp_highlight.UpdateMaterial(m_HighlightMaterial);

                    if (m_bEnableLook == true)
                    {
                        m_transGun.LookAt(m_hitInfo.point, m_transGun.up);
                    }
                  
                    Debug.DrawLine(ray.origin, m_hitInfo.point, Color.green);
                    Debug.DrawLine(m_transGun.position, m_hitInfo.point, Color.yellow);
                }
            }
            else
            {
                Reset();
                Debug.DrawRay(ray.origin, 5000.0f * ray.direction, Color.blue);
            }


        }
        private void Reset()
        {
            if (m_HitRender != null)
            {
                m_HitRender.UpdateMaterial(m_prevMAterial);
                m_HitRender = null;
                m_prevMAterial = null;
            }
        }
    }
}