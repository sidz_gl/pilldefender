﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GD
{
    public class Floor : MonoBehaviour
    {

        [SerializeField] private GameObject m_goFloor;
        [SerializeField] private int m_iXSize = 5;
        [SerializeField] private int m_iYSize = 5;
        [SerializeField] private float m_fSpacing = 5;

        [SerializeField] List<Transform> m_CreatedFloor;

        public List<Transform> CreatedFloor { get => m_CreatedFloor;  }

        // Start is called before the first frame update
        void Start()
        {
            Init();
        }

        private void Init()
        {
            GameObject temp_Gameobject = null;
            Vector3 temp_offset = Vector3.zero;
            for (int i = 0; i < m_iYSize; i++)
            {
                for (int j = 0; j < m_iXSize; j++)
                {
                    temp_offset = Vector3.right * m_fSpacing * j + -Vector3.forward * m_fSpacing * i;
                    temp_Gameobject = Instantiate(m_goFloor, transform.position, transform.rotation, transform);
                    temp_Gameobject.transform.position = transform.position + temp_offset;
                    m_CreatedFloor.Add(temp_Gameobject.transform);
                   

                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}