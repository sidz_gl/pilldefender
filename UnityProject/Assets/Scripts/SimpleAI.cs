﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GD
{
    public class SimpleAI : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField] private Floor m_refFloor;
        [SerializeField] private int m_iAttackIndex = 0;
        [SerializeField] private LineRenderer m_Laser = null;
        [SerializeField] private Transform m_turrentNozzle = null;

        [SerializeField] private float m_fHealth = 100;
        [SerializeField] private TextMesh m_txtMesh;

        private bool m_bStarted = false;

        private Vector3 m_vStartPos = Vector3.zero;
        public void ReduceHealth(float a_fHealthReduce)
        {
            m_fHealth -= a_fHealthReduce;
      
            if (m_fHealth <= 0)
            {
                Destroy(this.gameObject,0.25f);
            }

        }
        void Start()
        {
            m_refFloor = FindObjectOfType<Floor>();
            StartCoroutine(coAttackSequence());

            
        }

        // Update is called once per frame
        void Update()
        {
            m_fHealth = Mathf.Clamp(m_fHealth, 0, 100);
            m_txtMesh.text = m_fHealth.ToString("0.#");

            if(m_bStarted == true)
            {
                //transform.position = m_vStartPos + 10f * Vector3.right * Mathf.Sin(10f * Time.deltaTime);//  +0.01f*Vector3.up * Mathf.Sin(7.3f * Time.deltaTime);
                transform.LookAt(m_refFloor.CreatedFloor[m_iAttackIndex], transform.up);
                m_Laser.SetPosition(0, m_turrentNozzle.position);
                m_Laser.SetPosition(1, (m_refFloor.CreatedFloor[m_iAttackIndex].position));

            }
        }

        IEnumerator coAttackSequence()
        {
            yield return new WaitForSeconds(2.0f);
            m_iAttackIndex = Random.Range(0, m_refFloor.CreatedFloor.Count);
            transform.LookAt(m_refFloor.CreatedFloor[m_iAttackIndex], transform.up);
            m_Laser.SetPosition(0, m_turrentNozzle.position);
            m_Laser.SetPosition(1, (m_refFloor.CreatedFloor[m_iAttackIndex].position));
            m_vStartPos = transform.position;
            m_bStarted = true;

            while (true)
            {
                yield return new WaitForSeconds(5.0f);

                m_iAttackIndex = Random.Range(0, m_refFloor.CreatedFloor.Count);
                transform.LookAt(m_refFloor.CreatedFloor[m_iAttackIndex], transform.up);
                m_Laser.SetPosition(0, m_turrentNozzle.position);
                m_Laser.SetPosition(1, (m_refFloor.CreatedFloor[m_iAttackIndex].position));
            }


        }
    }
}