﻿using System.Collections.Generic;
using UnityEngine;
namespace GD
{
    public class EnemyManager : MonoBehaviour
    {
        [SerializeField] private GameObject m_SimpleAI;
        [SerializeField] private int m_iCount = 8;

        [SerializeField] private List<Transform> m_SpawnPlane;

        [SerializeField] private List<Transform> m_CreatedAI;
        // Start is called before the first frame update
        void Start()
        {
            Init();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space) == true)
            {
                SpawnEnemy();
            }
        }

        void Init()
        {
            SpawnEnemy();
        }


        private void SpawnEnemy()
        {
            m_CreatedAI.Clear();
            GameObject temp_createdAI = null;
            int temp_iPlane = Random.Range(0, 3);
            Vector3 offset = Vector3.zero;
            for (int i = 0; i < m_iCount; i++)
            {
                offset = m_SpawnPlane[temp_iPlane].position + Random.Range(-3.0f, 3.0f) * m_SpawnPlane[temp_iPlane].right
                                                            + Random.Range(-1.5f, 1.5f) * m_SpawnPlane[temp_iPlane].up;
                temp_createdAI = Instantiate(m_SimpleAI, offset, m_SimpleAI.transform.rotation);
                temp_iPlane = Random.Range(0, 3);
                m_CreatedAI.Add(temp_createdAI.transform);
            }

        }
    }
}