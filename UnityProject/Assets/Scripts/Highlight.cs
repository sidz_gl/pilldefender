﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GD
{
    public class Highlight : MonoBehaviour
    {
        [SerializeField] private Renderer m_renderer;

        private void Start()
        {

        }
        public void UpdateMaterial(Material a_netMat)
        {
            m_renderer.material = a_netMat;
        }
        public Material GetMaterial()
        {
            return m_renderer.material;
        }
    }
}