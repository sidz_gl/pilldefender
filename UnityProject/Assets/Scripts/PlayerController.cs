﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GD
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Transform m_transBase = null;
        [SerializeField] private Transform m_transGun = null;
        [SerializeField] private Transform m_transGunNozzle = null;
        [SerializeField] private Bullet m_goGunBullets = null;
        [SerializeField] private LayerMask m_RayLayer;
        [SerializeField] private LineRenderer m_Laser = null;



        [Range(0.0f, 25.0f)]
        [SerializeField] private float m_fSpeed = 0.5f;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Movement();
            // TurretFollow();
            //Shoot();
            //  RayHitRegion();
        }

        private void Movement()
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(m_transBase.up * Time.deltaTime * m_fSpeed);
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(m_transBase.up * Time.deltaTime * -m_fSpeed);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(m_transBase.right * Time.deltaTime * -m_fSpeed);
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(m_transBase.right * Time.deltaTime * m_fSpeed);
            }
        }

        void TurretFollow()
        {
            //Input.mousePosition
            var mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);


            Debug.Log(mouseRay);

            //  m_transGun.LookAt(100.0f * mouseRay.direction, transform.up);
            //m_transGun.LookAt(mouseRay.origin + 40.0f * mouseRay.direction, transform.up);



        }

        void Shoot()
        {
            if (Input.GetMouseButton(0))
            {
               // var spawnedBullet = Instantiate(m_goGunBullets, m_transGunNozzle.position, m_transGunNozzle.rotation).GetComponent<Bullet>();
                //spawnedBullet.Shoot(m_transGun.forward);
                RayHitRegion();
                m_Laser.enabled = true;
            }
            else
            {
                m_Laser.enabled = false;
            }

            Debug.DrawRay(m_transGunNozzle.position, m_transGunNozzle.forward * 1000, Color.blue);

            //m_Laser.SetPosition(0, m_transGunNozzle.position);
            //  m_Laser.SetPosition(1, m_transGunNozzle.forward * 100.0f);
        }

        void RayHitRegion()
        {
            Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit m_result;
            if (Physics.Raycast(mouseRay, out m_result, 500) == true)
            {
                //m_Laser.SetPosition(0, m_transGunNozzle.position);
                //m_Laser.SetPosition(1, m_result.point);
                Debug.Log("Tag Value:"+m_result.collider.gameObject.tag+","+ m_result.collider.gameObject.name);
                if (m_result.collider.gameObject.tag == "Enemy")
                {
                    var ai = m_result.collider.gameObject.GetComponent<SimpleAI>();
                    if (ai != null)
                    {
                        ai.ReduceHealth(50);
                    }
                }
                else
                {


                }


               // m_transGun.LookAt(m_result.point, transform.up);
                m_transGun.LookAt(mouseRay.origin + 1000 * mouseRay.direction, transform.up);
            }
            else
            {
                // m_Laser.SetPosition(0, m_transGunNozzle.position);
                // m_Laser.SetPosition(1, m_transGunNozzle.forward * 100.0f);
                m_transGun.LookAt(mouseRay.origin + 1000 * mouseRay.direction, transform.up);
            }
            m_Laser.SetPosition(0, m_transGunNozzle.position);
            m_Laser.SetPosition(1, mouseRay.direction*1000);
            Debug.DrawRay(mouseRay.origin, 40.0f * mouseRay.direction, Color.red);
        }

    }
}